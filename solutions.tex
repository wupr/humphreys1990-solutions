\documentclass{article}

% keep the next two macro definitions on lines 5 and 6
% for `sed` to substitute the version parameters 
\newcommand*{\versionshortsha}{[COMMIT-SHORT-SHA]}
\newcommand*{\versiondate}{[DATE]}

% document geometry
\usepackage[a4paper, margin=2.54cm]{geometry}

% input encoding
\usepackage[utf8]{inputenc}

% spacing
% \usepackage{parskip}
\usepackage{setspace}
% \renewcommand*{\arraystretch}{1.5}

% hyphenation
\usepackage[UKenglish]{babel}

% cross-referencing
\usepackage[colorlinks]{hyperref}

% section-numbering style
% \usepackage{titlesec}

% lists
\usepackage{enumitem}
\setlist[enumerate,1]{label=(\alph*)}

% mathematics
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{stmaryrd}
\usepackage[mark=o]{dynkin-diagrams}

% theorems
\theoremstyle{definition}
\newtheorem{exercise}{Exercise}[subsection]
\renewcommand{\theexercise}{\arabic{exercise}}
\newtheorem*{exercise*}{Exercise}

% TikZ
% \usepackage{tikz-cd}

% foreign
\usepackage{foreign}
\newcommand*{\ie}{\foreign{i.e.}}
% \newcommand*{\eg}{\foreign{e.g.}}
% \newcommand*{\cf}{\foreign{cf.}}

% misc
% \newcommand*{\proving}[1]{\underline{#1}:\;}

% header and footer
% \usepackage{fancyhdr}
% \pagestyle{fancy}
% \fancyhf{}
%
% \lhead{}
% \rhead{}
% \lfoot{}
% \usepackage{lastpage}
% \rfoot{Page~\thepage~of~\pageref*{LastPage}}
%
% alternatively
% \fancyhead[LO, RE]{}
% \fancyhead[RO, LE]{}
% \fancyfoot[LO, RE]{}
% \fancyfoot[RO, LE]{}

% document-specific macros
\newcommand*{\noexercises}[1]{%
    \stepcounter{subsection}%
    \noindent \emph{No exercises in \thesubsection~#1.}%
}
\newcommand*{\skipped}{\emph{Skipped.}}
\newcommand*{\RR}{\mathbf{R}}
\newcommand*{\ZZ}{\mathbf{Z}}
\DeclareMathOperator{\Orth}{O}
\DeclareMathOperator{\Card}{Card}

% metadata
\title{Solutions to the Exercises in Reflection Groups and Coxeter Groups}


\begin{document}

\begin{center}
    \setstretch{1.5}
    \setlength\parindent{0pt}

    \textbf{\Large Solutions to the Exercises in}

    \textbf{\LARGE Reflection Groups and Coxeter Groups}

    {\large Peiran Wu}

    {\large (Author of the book: James E. Humphreys)}

    \vspace{1em}

    \emph{Work in Progress}

    Version \texttt{\versionshortsha} (\versiondate)

    \vspace{2em}
\end{center}

\section*{Introduction}

This is a set of solutions to the exercises that appear in Humphreys's 1990 book, \textit{Reflection Groups and Coxeter Groups}.
Throughout the article, I have tried to adhere to the notation and numbering adopted by Humphreys.

If you spot a mistake, please let me know; but also feel free to get in touch with any other comments or questions.

\vspace{0.5em}

\emph{
    This article is generated from a TeX file and is available in both HTML and PDF versions.
    More information as well as the source code is available on the \href{https://gitlab.com/wupr/humphreys1990-solutions/}{project website}.
}

\subsection*{Updating}

I aim to update this article regularly, with new solutions added and existing solutions modified/corrected.
For more information, see the project website linked to above.

\tableofcontents


\section{Finite reflection groups}

\subsection{Reflections}

\begin{exercise}
    \skipped
\end{exercise}

\begin{exercise}
    \skipped
\end{exercise}

\noexercises{Roots}

\subsection{Positive and simple systems}

\begin{exercise}
    One may take the ambient vector space to be \(\RR^2\).
    Let \(\alpha, \beta\) be the roots in \(\Phi\).
    Note that \((\alpha, \beta) \leq 0\).
    There cannot be any root in \(\Phi\) that lie within the acute-angled (or right-angled) regions between the lines \(\RR \alpha\) and \(\RR \beta\).

    If the two lines are perpendicular, then there are no further roots and \(W = \mathcal{D}_2 = \ZZ_2 \times \ZZ_2\).

    If they are not perpendicular, then the obtuse angle between the two lines must be an integer multiple of the acute angle between them (otherwise, applying a finite sequence of \(s_\alpha\)'s and \(s_\beta\)'s to \(\alpha\) or \(\beta\) would result in a root in one of the acute-angled regions).
    In this case, \(\Phi\) has exactly \(2m\) elements (\(m \geqslant 3\)) and it is clear that \(W = \mathcal{D}_m\).
    \qed
\end{exercise}

\begin{exercise} \label{ex:1.3.2}
    (\(\mathrm{I}_2(m)\), \(m \geq 3\)) Order \(\RR^2\) with \((1, 0) > (0, 1)\).
    A simple system is \(\{ \alpha, \beta \}\), as given on page 4 of the book.

    (\(\mathrm{A}_{n-1}\), \(n \geq 2\)) Regard \(\mathcal{S}_n\) as a subgroup of \(\Orth(n, \RR)\) in the way specified on page 5 of the book.
    Order \(\RR^n\) with \(\varepsilon_1 > \cdots > \varepsilon_n\).
    A simple system is \(\{ \varepsilon_1 - \varepsilon_2, \ldots, \varepsilon_{n-1} - \varepsilon_n \}\).

    (\(\mathrm{B}_n\), \(n \geq 2\)) Using the same ordering as above, a simple system is \(\{ \varepsilon_1 - \varepsilon_2, \ldots, \varepsilon_{n-1} - \varepsilon_n, \varepsilon_n \}\).

    (\(\mathrm{D}_n\), \(n \geq 4\)) Still using the same ordering as above, a simple system is \(\{ \varepsilon_1 - \varepsilon_2, \ldots, \varepsilon_{n-1} - \varepsilon_n, \varepsilon_{n-1} + \varepsilon_n \}\).
\end{exercise}

\noexercises{Conjugacy of positive and simple systems}

\subsection{Generation by simple reflections}

\begin{exercise}
    One may assume that \(W\), the reflection group arising from the root system \(\Phi\), is essential relative to the ambient vector space \(V\).
    Suppose there is a simple system \(\Delta = \{ \alpha_1, \ldots, \alpha_n \} \subset \Phi\) (which is a basis of \(V\)) and a subset of roots \(\Psi = \{ \alpha'_1, \ldots, \alpha'_n \} \subset \Phi\) such that \((\alpha_i, \alpha_j) = (\alpha'_i, \alpha'_j)\) for all \(i, j \in \{ 1, \ldots, n \}\).
    (This is a slightly weaker assumption than that in the original exercise, where all roots are unit vectors).
    Then there is a unique \(t \in \Orth(V)\) such that, for all \(i \in \{ 1, \ldots, n \}\), \(t \alpha_i = \alpha'_i\).

    Clearly, \(\Psi = t \Delta\) is a simple system in \(t \Phi\).
    It suffices to show that \(t \Phi = \Phi\).\footnotemark
    \footnotetext{The fact that one may try to prove this sufficient condition is a hint I obtained from \href{https://math.stackexchange.com/a/2243442}{an StackExchange answer} by \href{https://math.stackexchange.com/users/300783/jendrik-stelzner}{Jendrik Stelzner}, although I came up with the rest of the proof by myself.}

    Let \(\gamma \in \Phi\).
    By Corollary 1.5, there is \(\alpha \in \Delta\) and \(\beta_1, \ldots, \beta_m \in \Delta\) such that
    \[ s_{\beta_1} \cdots s_{\beta_m} \gamma = \alpha. \]
    Then
    \[ (t s_{\beta_1} \cdots s_{\beta_m} t^{-1}) t \gamma = t \alpha. \]
    Since \(t\) is an orthogonal map, this may be rewritten as
    \[ s_{t \beta_1} \cdots s_{t \beta_m} t \gamma = t \alpha. \]
    Since \(t \alpha, t \beta_1, \ldots, t \beta_m \in \Psi \subset \Phi\), one has \(t \gamma \in \Phi\).
    This proves that \(t \Phi \subset \Phi\).
    Since \(\Phi\) is finite and \(t\) is injective, one has \(t \Phi = \Phi\), as desired.
    \qed
\end{exercise}

\begin{exercise} \label{ex:1.5.2}
    Suppose for a contradiction that there is \(\alpha \in \Delta\) such that \(s_\alpha\) is redundant as a generator among the simple reflections.
    By Corollary 1.5, there is \(w \in W\) such that \(w(-\alpha) \in \Delta\).
    Then \(w = s_{\alpha'_1} \cdots s_{\alpha'_m}\) for some \(\alpha'_1, \ldots, \alpha'_m \in \Delta \setminus \{\alpha\}\).
    Then \(w(-\alpha) = -\alpha + c_{\alpha'_1} \alpha'_1 + \cdots c_{\alpha'_m} \alpha'_m\).
    Since \(\Delta\) is linearly independent and the coefficient of \(\alpha\) in the above linear combination is \(-1\), one finds that \(w(-\alpha)\) is a negative root \(\lightning\).
    \qed
\end{exercise}

\begin{exercise}
    Suppose \(\beta \in \Pi \setminus \Delta\).
    By step (i) in the proof of Theorem 1.5, elements of the smallest possible height in \(W \beta \cap \Pi\) are simple roots, which are of height \(1\).
    Since \(\beta\) is not a simple root, one finds \(\operatorname{ht}(\beta) > 1\).
    \qed
\end{exercise}

\subsection{The length function}

\begin{exercise}
    \begin{enumerate}
        \item Clearly, \(n(1) = 0\) and \(\det(1) = 1 = (-1)^0\).
              If \(w = w' s_\alpha\), then \(\det(w) = -\det(w')\) and by Lemma 1.6(a)(b), \(n(w) = n(w') \pm 1\).
              Therefore, by induction, \(\det(w) = (-1)^{n(w)}\).

        \item First, we prove the statement that \(n(w w') \leq n(w) + n(w')\).
              In the notation of the proof of Lemma 1.6, this is equivalent to that \(\Card(\Pi(w w')) \leq \Card(\Pi(w)) + \Card(\Pi(w'))\).
              Let \(\beta \in \Pi(w w')\), \ie \(\beta\) is a positive root sent to a negative root by \(w w'\).
              Either \(w' \beta \in \Pi\) or \(w' \beta \in -\Pi\).
              In the latter case, \(\beta \in \Pi(w')\); in the former, \(w' \beta \in \Pi \cap w^{-1}(-\Pi)\), so \(\beta \in {w'}^{-1} \Pi(w)\).
              Therefore, \(\Pi(w w') \subset \Pi(w') \sqcup {w'}^{-1} \Pi(w)\), which implies the desired statement.

              The parity statement follows from part (a) and the multiplicativity of the determinant function.
              \qed
    \end{enumerate}
\end{exercise}

\begin{exercise}
    Recall the simple system for \(\mathcal{S}_n\) from the \hyperref[ex:1.3.2]{solution to Section 1.3, Exercise 2}.
    For \(i \in \{ 1, \ldots, n - 1 \}\), the transposition \((i~i+1)\) acts as the reflection that swaps \(\varepsilon_i\) and \(\varepsilon_{i + 1}\) (fixing the other standard basis vectors).
    A permutation \(\pi \in \mathcal{S}_n\) sends a positive root \(\varepsilon_i - \varepsilon_j\) (\(i < j\)) to \(\varepsilon_{\pi(i)} - \varepsilon_{\pi(j)}\), which is a negative root if and only if \(\pi(i) > \pi(j)\).
    Therefore, the length of \(\pi\) is the number of inversions of \(\pi\).
\end{exercise}

\subsection{Deletion and Exchange Conditions}

\begin{exercise}
    Suppose there are \(i \neq j\) with \(s_1 \cdots \hat{s_i} \cdots s_r s = w = s_1 \cdots \hat{s_j} \cdots s_r s\).
    Then \(s_{i + 1} \cdots s_j = s_i \cdots s_{j - 1}\).
    This implies \(w = s_1 \cdots \hat{s_i} \cdots \hat{s_j} \cdots s_r\) \(\lightning\).
    \qed
\end{exercise}

\begin{exercise}
    \itshape
    Let \(w = s_1 \cdots s_r\) (not necessarily reduced), where each \(s_i\) is a simple reflection.
    If \(\ell(sw) < \ell(w)\) for some simple reflection \(s = s_\alpha\), then there exists an index \(i\) for which \(sw = s_1 \cdots \hat{s_i} \cdots s_r\) (and thus \(w = s s_1 \cdots \hat{s_i} \cdots s_r\), with a factor \(s\) substituted for a factor \(s_i\)).
    In particular, \(w\) has a reduced expression starting in \(s\) if and only if \(\ell(s w) < \ell(w)\).
\end{exercise}

\subsection{Simple transitivity and the longest element}

\begin{exercise} \label{ex:1.8.1}
    \(w_\circ = (1~n) (2~n-1) (3~n-2) (\lfloor n/2 \rfloor~\lceil n/2 \rceil)\).
    The length of \(w_\circ\) is \(n (n - 1) / 2\) and one reduced form is \(w_\circ = (1~2) \cdots (n-1~n) (1~2) \cdots (n-2~n-1) (1~2) \cdots (n-3~n-2) \cdots (1~2) (2~3) (1~2)\).

    (Since \(w_\circ^{-1} = w_\circ\), one can read this reduced form from left to right or from right to left.
    When read from left to right, a ``\href{https://en.wikipedia.org/wiki/Bubble_sort}{bubble sort}'' is performed on a reverse-sorted array of size \(n\); in the other direction, it is a ``\href{https://en.wikipedia.org/wiki/Gnome_sort}{gnome sort}''.)
\end{exercise}

\begin{exercise}
    Suppose \(s_\alpha\) does not appear in a reduced form of \(w_\circ\), where \(\alpha \in \Delta\).
    Repeating the argument of the \hyperref[ex:1.5.2]{solution to Section 1.5, Exercise 2}, one can write \(w_\circ(\alpha)\) as a linear combination of \(\Delta\) with a positive coefficient of \(\alpha\).
    So \(w_\circ(\alpha) \notin -\Pi\) \(\lightning\).
    \qed
\end{exercise}

\noexercises{Generators and relations}

\subsection{Parabolic subgroups and minimal coset representatives}

\begin{exercise}
    It depends on what one tolerates as ``analogous''.
    The following statement can be proven in a way similar to the proof given in the book:

    \textit{
        Define $^I W ^J$ \(\coloneqq \{ w \in W \mid \ell(s w t) > \ell(w) \text{ for all } s \in I, t \in J \}\).
        Given \(w \in W\), \textbf{if \(\Card(W_I w \cap w W_J) = 1\)}, then there is a unique tuple \((v_I,  u, v_J) \in W_I \times {^I W ^J} \times W_J\) such that \(w = v_I u v_J\).
        Moreover, \(\ell(w) = \ell(v_I) + \ell(u) + \ell(v_J)\) and \(u\) is the unique element of smallest length in the double coset \(W_I w W_J\).
    }

    The additional condition that \(\Card(W_I w \cap w W_J) = 1\) implies that \(W_I w W_J\) and \(W_I \times W_J\) are bijective; it also resolves an extra scenario that arises when applying the Deletion Condition in the proof.
\end{exercise}

\begin{exercise}
    The statement is trivial when \(n = 2\).

    Suppose \(n > 2\).
    Let \(I \subset S = \{ (i~i+1) \mid i = 1, \ldots, n - 1 \}\).
    If \(I = S\), then \(W_I = \mathcal{S}_n\) is a trivial direct product of symmetric groups.
    Otherwise, \((j~j+1) \notin I\), say.
    Define \(I_\leq \coloneqq I \cap \{ (i~i+1) \mid i = 1, \ldots, j \}\) and \(I_\geq \coloneqq I \cap \{ (i~i+1) \mid i = j+1, \ldots, n-1 \}\).
    Clearly, \(W_{I_\leq}\) and \(W_{I_\geq}\) are normal subgroups of \(W_I\) that together generate \(W_I\).
    Furthermore, considering the canonical free action of \(\mathcal{S}_n\) on \(X \coloneqq \{ 1, \ldots, n \}\), one sees that \(W_{I_\leq}\) fixes \(j + 1, \ldots, n \in X\), whereas \(W_{I_\geq}\) fixes \(1, \ldots, j \in X\).
    So \(W_{I_\leq} \cap W_{I_\geq}\) acts trivially on \(X\) and must hence be trivial.
    Therefore, \(W_I = W_{I_\leq} \times W_{I_\geq}\).
    Now, \(W_{I_\leq}\) and \(W_{I_\geq}\) are either trivial or isomorphic to a parabolic subgroup of \(\mathcal{S}_m\) for some \(2 \leq m < n\).
    The desired statement follows by induction.
    \qed
\end{exercise}

\begin{exercise}
    Consider \(W_I\), where \(I = \{ s, s' \}\).
    It suffices to show that \(w v \in W^I\), since, by Proposition 1.10(c), this implies that \(\ell(w) = \ell(w v) + \ell(v) = \ell(w v) + m\).

    Let \(\alpha, \alpha'\) be the simple roots corresponding to \(s, s'\) respectively.
    By the assumptions and Lemma 1.6, \(w \alpha, w \alpha' < 0\).
    Since \(\ell(v s) = \ell(v s') = m - 1 < \ell(v)\), one has \(v \alpha, v \alpha' < 0\).
    Since \(v \in W_I\), the negative roots \(v \alpha, v \alpha'\) must be non-positive linear combinations of \(\alpha, \alpha'\).
    So \(w (v \alpha), w (v \alpha') > 0\).
    Using Lemma 1.6 again, it is easy to see that \(w v \in W^I\).
    \qed
\end{exercise}

\subsection{Poincaré polynomials}

\begin{exercise}
    When \(W = \mathcal{S}_3\), by the \hyperref[ex:1.8.1]{solution to Section 1.8, Exercise 1}, the longest element is \((1~3)\), of length \(3\).
    Proposition 1.11 gives the equation
    \[ (-1)^0 \frac{W(t)}{1} + 2 (-1)^1 \frac{W(t)}{1 + t} + (-1)^2 \frac{W(t)}{W(t)} = t^3. \]
    So \(W(t) = (1 + t) (t^3 - 1) / (t - 1) = 1 + 2 t + 2 t^2 + t^3\).

    When \(W = \mathcal{D}_m\), the size of any positive system is \(m\), so \(W(t) = (1 + t) (t^m - 1) / (t - 1) = 1 + 2 t + 2 t^2 + \cdots + 2 t^{m - 1} + t^m\).
\end{exercise}

\begin{exercise}
    The identity becomes
    \[ (-1)^0 \frac{\Card(W)}{1} + 3 (-1)^1 \frac{\Card(W)}{2} + (-1)^2 \left( \frac{1}{4} + \frac{1}{6} + \frac{1}{10} \right) \Card(W) + (-1)^3 \frac{\Card(W)}{\Card(W)} = 1. \]
    So \(\Card(W) = 120\).
\end{exercise}

\subsection{Fundamental domains}

\begin{exercise}
    A product of simple reflections is equal to \(1\) in \(W\) if and only if it fixes any single \(\lambda \in C\).
\end{exercise}

\begin{exercise}
    By Theorem 1.12(a), the isotropy group of any \(\lambda \in D\) is generated by the simple reflections it contains.
    The subgroup \(W^0\) of \(W\) fixing pointwise a basis \(\lambda_1, \ldots, \lambda_t \in U\) of the span of \(U \subset D\) is clearly the same as the subgroup fixing \(U\) pointwise.
    The rest of the proof is similar to the proof of Theorem 1.12(d) that is given in the book.
\end{exercise}

\begin{exercise}
    \emph{To be added.}
\end{exercise}

\begin{exercise}
    A few notes first.
    With the new partial ordering introduced in the proof of Lemma 1.12, for any \(\alpha \in \Phi\), \(\alpha \in \Pi\) if and only if \(\alpha > 0\).
    It is easy to see that \(C = \bigcap_{\beta \in \Pi} A_\beta\) and \(wC = \bigcap_{\beta \in \Pi} A_{w \beta}\).
    Note that for each \(\beta \in \Pi\), there exists a unique \(\alpha \in \Pi\) such that \(A_{w \beta} = \pm A_\alpha\); if \(w \beta \in -\Pi\), then \(\alpha = -w \beta > 0\) and \(H_{\alpha}\) ``separates'' \(C\) and \(wC\).

    Now, for any \(\lambda \in C\) and any \(\alpha \in \Pi\), \(H_\alpha\) separates \(C\) and \(wC\) if and only if \(0 > (w \lambda, \alpha) = (\lambda, w^{-1} \alpha)\) if and only if \(\alpha \in \Pi \cap w(-\Pi)\).
    So the number of hyperplanes \(H_\alpha (\alpha \in Pi)\) which separate \(C\) and \(wC\) equals \(n(w^{-1}) = n(w) = \ell(w)\).
    \qed
\end{exercise}

\subsection{The lattice of parabolic subgroups}

\begin{exercise*}
    \(s_r \alpha_r = -\alpha_r\).
    Since \(s_1, \ldots, s_r\) are distinct, \(s_1 \cdots s_r \alpha_r\) is negative root  (see the argument in the \hyperref[ex:1.5.2]{solution to Section 1.5, Exercise 2}).
    By Lemma 1.6, \(\ell(s_1 \cdots s_r) = \ell(s_1 \cdots s_{r - 1}) + 1\).
    It follows by induction that \(\ell(s_1 \cdots s_r) = r\).
\end{exercise*}

\subsection{Reflections in \texorpdfstring{$W$}{W}}

\begin{exercise*}
    It is easy to check that \(\Phi'\) satisfies the properties stated in the hint.
    It is also clear that \(\Phi' \subset \Phi\).
    Now, suppose \(\beta \in \Phi\).
    Applying Proposition 1.14 to \(\Phi'\), one sees that \(s_\beta = s_{w\alpha}\) for some \(w \in W\) and \(\alpha \in \Psi\).
    So \(\beta\) and \(w \alpha\) are parallel.
    Since \(\Phi\) is a root system and \(w \alpha \in \Phi\), we have \(\beta = \pm w \alpha\); hence \(\beta \in \Phi'\).
    This proves \(\Phi' = \Phi\).
    Thus follows the statement that every element of \(\Phi\) is \(W\)-conjugate to some element of \(\Psi\).
\end{exercise*}

\noexercises{The Coxeter complex}

\noexercises{An alternating sum formula}

\section{Classification of finite reflection groups}

\noexercises{Isomorphisms}

\subsection{Irreducible components}

\begin{exercise*}
    The Coxeter graph for \(S\) is \dynkin[Coxeter,gonality=6]{I}{2}, which is connected.
    The Coxeter graph for \(S'\) is \dynkin[Coxeter,gonality=3]{I}{2} \dynkin[Coxeter]{A}{1}, which is disconnected.
\end{exercise*}

\noexercises{Coxeter graphs and associated bilinear forms}

\noexercises{Some positive definite graphs}

\noexercises{Some positive semidfinite graphs}

\noexercises{Subgraphs}

\noexercises{Classification of graphs of positive type}

\subsection{Crystallographic groups}

\begin{exercise*}
    Suppose \(m(\alpha, \beta)\) is \(2\), \(3\), \(4\), or \(6\) when \(\alpha \neq \beta\) in \(\Delta\).
    Assume the fact that the Coxeter graph has no circuits (for finite \(W\)).
    It is possible and sufficient to modify the lengths of the simple roots such that for any \(\alpha, \beta \in \Delta\),
    \begin{enumerate}
        \item if \(m(\alpha, \beta) = 3\), then \((\alpha, \alpha) / (\beta, \beta) = 1\);
        \item if \(m(\alpha, \beta) = 4\), then \((\alpha, \alpha) / (\beta, \beta)\) is \(2\) or \(1/2\);
        \item if \(m(\alpha, \beta) = 6\), then \((\alpha, \alpha) / (\beta, \beta)\) is \(3\) or \(1/3\).
    \end{enumerate}

    For each connected component in the Coxeter graph, consider the corresponding subset of simple roots; fix the length of one of the simple roots, modify the length of every 'adjacent' simple root that doesn't already satisfy the property above, and repeat to propagate the property to the entire connected component.
    This process terminates without conflicts since there are no circuits in the graph.

    With the modified lengths, for any \(\alpha, \beta \in \Delta\),
    \[ s_\beta \alpha = \alpha - \frac{2(\alpha, \beta)}{(\beta, \beta)} \beta = \alpha + 2 \left(\cos \frac{\pi}{m(\alpha, \beta)}\right) \sqrt{\frac{(\alpha, \alpha)}{(\beta, \beta)}} \beta; \]
    it is now easy to check that this is either \(\alpha\) (when \(m(\alpha, \beta) = 2\)), \(-\alpha\) (when \(\alpha = \beta\)), or a proper \(\ZZ\)-linear combination of \(\alpha\) and \(\beta\).
    By induction, this implies that the \(\ZZ\)-span of the simple roots is now stable under \(W\).
\end{exercise*}

\end{document}
