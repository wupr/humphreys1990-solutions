This is a personal project where I write up in an article solutions to the exercises that appear in Humphreys's 1990 book, *Reflection Groups and Coxeter Groups*.

The article is written in TeX (see the [source file][tex]) and is available both as a [**webpage**][html] and as a [**PDF file**][pdf].

The project is work in progress and I aim for regular updates.
The links above should always take you to the latest version.

[tex]: solutions.tex
[html]: https://wupr.gitlab.io/humphreys1990-solutions/
[pdf]: https://wupr.gitlab.io/humphreys1990-solutions/humphreys1990-solutions.pdf

[[_TOC_]]

## Background and motivation

I started writing these solutions during the seminar course, *Seminar zur Algebra*, at Humboldt-Universität zu Berlin (HU Berlin) in Summer Semester 2021.
The seminar, in which I took part, was led by Prof. Dr. Elmar Große-Klönne and followed the first few chapters of Humphreys's book.
Amid the Covid-19 pandemic (often referred to as *die aktuelle Situation* here in Germany) and related social restrictions, the seminar was held online.

Although it was not a requirement of the course, I was and still am attempting most of the exercises in the book.
By publishing this article online, I hope that fellow participants of the seminar – and indeed anyone else – could point out mistakes I have made, discover their own, get hints when stuck, share different approaches, or have a general discussion about the book or subject.

## Mistakes and discussions

If you spot a mistake in my solutions, please let me know; but also feel free to get in touch with any other comments or questions.
You can email me at peiran.wu@campus.tu-berlin.de or create an issue here on GitLab.

~~If you are a fellow participant of the seminar, starting discussions on Moodle is probably still a better option, as it is easier for other participants to see or join in there.~~
